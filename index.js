function myFunction() {
  let x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function phoneNum(){
    let x = document.getElementById("flag").value;
    if (x == 1){
        document.getElementById("phoneNumber").setAttribute("placeholder", "+7");
    }
    else if (x == 2){
        document.getElementById("phoneNumber").setAttribute("placeholder", "+998");
        console.log("Hello")
    }
    else if (x == 3){
        document.getElementById("phoneNumber").setAttribute("placeholder", "+45");
    }
    else if (x == 4){
        document.getElementById("phoneNumber").setAttribute("placeholder", "+90");
    }
    else if (x == 5){
        document.getElementById("phoneNumber").setAttribute("placeholder", "+44");
    }
}

function borderColor1(){
    let x = document.getElementById("nameFull").value;

    {if (x !==""){
        document.getElementById("nameFull").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";
      }
    }

    {
        if (x === ""){
            document.getElementById("nameFull").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";

        }
    }
}

function borderColor2(){
    let x = document.getElementById("age").value;

    {if (x !==""){
        document.getElementById("age").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }
    {
        if (x === ""){
            document.getElementById("age").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor3(){
    let x = document.getElementById("priceTotal").value;

    {if (x !==0){
        document.getElementById("priceTotal").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }
    {
        if (x == 0){
            document.getElementById("priceTotal").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor4(){
    let x = document.getElementById("pricePaid").value;

    {if (x !==0){
        document.getElementById("pricePaid").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }

    {
        if (x == 0){
            document.getElementById("pricePaid").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor5(){
    let x = document.getElementById("year").value;

    {if (x>1959 && x<2010){
        document.getElementById("year").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }
    {
        if (x == 0){
            document.getElementById("year").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor6(){
    let x = document.getElementById("accoutName").value;

    {if (x !==""){
        document.getElementById("accoutName").style.border = "1px solid #79d0f8";
        document.getElementById("emailChoice").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }

    {
        if (x === ""){
            document.getElementById("accoutName").style.border = "1px solid #eaa661";
            document.getElementById("emailChoice").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor7(){
    let x = document.getElementById("password").value;

    {if (x !==""){
        document.getElementById("password").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    }
    }

    {
        if (x === ""){
            document.getElementById("password").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

function borderColor8(){
    let x = document.getElementById("passwordRe").value;
    let z = document.getElementById("password").value;

    if (x!==z){
        document.getElementById("passwordRe").style.border = "1px solid red";
        document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        document.getElementById("alertText").style.color = "red";


    } else if ((x ===z ) && (x !== "")){
        document.getElementById("passwordRe").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";

    } else if ((x ===z ) && (x === "")){
        document.getElementById("passwordRe").style.border = "1px solid #eaa661";
        document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
    }
}

function borderColor9(){
    let x = document.getElementById("phoneNumber").value;

    {if (x !==""){
        document.getElementById("phoneNumber").style.border = "1px solid #79d0f8";
        document.getElementById("flag").style.border = "1px solid #79d0f8";
        document.getElementById("alertText").innerText = "";
    }
    }

    {
        if (x === ""){
            document.getElementById("phoneNumber").style.border = "1px solid #eaa661";
            document.getElementById("flag").style.border = "1px solid #eaa661";
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    }
}

$(document).ready(function (){
    $("input").keypress(function (e){
        var x = $(this).val() + String.fromCharCode(e.keyCode);

        if (x !== "") {
            $(this).css("border", "1px solid #79d0f8");
        }

        if (x === "") {
            $(this).css("border", "1px solid #eaa661");
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    });

    $("input").change(function (e){
        var x = $(this).val() + String.fromCharCode(e.keyCode);

        if (x !== "") {
            $(this).css("border", "1px solid #79d0f8");
        }

        if (x === "") {
            $(this).css("border", "1px solid #eaa661");
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    });

    $("select").click(function (e){
        var x = $(this).val() + String.fromCharCode(e.keyCode);

        if (x!=="") {
            $(this).css("border", "1px solid #79d0f8");
        } else {
            $(this).css("border", "1px solid #eaa661");
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    });

    $("select").change(function (e){
        var x = $(this).val() + String.fromCharCode(e.keyCode);

        if (x!=="") {
            $(this).css("border", "1px solid #79d0f8");
        } else {
            $(this).css("border", "1px solid #eaa661");
            document.getElementById("alertText").innerText = "* Please, fill out the gaps correctly!";
        }
    });
});  //jQuery border styling


