function formatNumber(number)
{
    number = number + "";
	if( Number(number.indexOf(".")) > 0 && number.length - Number(number.indexOf(".")) > 2 )
		number = Number(number).toFixed(2) + '';
    
	let ArrayNumber = number.split('.');
    let number1 = ArrayNumber[0];
    let number2 = (ArrayNumber.length > 1) ? ('.' + ArrayNumber[1] ) : ('');
    let rgx = /(\d+)(\d{3})/;
    while (rgx.test(number1)) {
        number1 = number1.replace(rgx, '$1' + ',' + '$2'); 
    }
    return number1 + number2;}

function notZero(n)
{
    n = +n;  // Coerce to number.
    if (!n) {  // Matches +0, -0, NaN
        document.getElementById("result").innerText = "Zero divide cannot be";
        throw new Error('Invalid dividend ' + n);
    }
    return n;
}  // Zero Divide Exception

$(document).ready(function() {
    $(document).on("keypress", "#price-total, #price-paid, #price-total-1, #price-paid-1", function(event){
        var x = $(this).val().replaceAll(",","").replaceAll("$", "") + String.fromCharCode(event.keyCode);
        if ( isNaN( String.fromCharCode(event.keyCode) )){
            if (event.keyCode === 46)
                return true;
            else
                return false;
        }

        if ((x + "").length >9)
            return false;

        if (x.indexOf(".") !== -1){
            if ( x.length - (x.indexOf(".") + 1) <= 2)
                return true;

            if ( x.length - (x.indexOf(".") + 1) > 2)
                return false;}
    })
    $(document).on("keyup", "#price-total, #price-paid, #price-total-1, #price-paid-1", function (){
        let x = $(this).val();
        x = x.replaceAll(",","");
        x = x.replaceAll(/\s/g, "");
        $(this).val(formatNumber(x));
    })
    $("#buttonResult").click(function (){
        let input1 = $("#input1").val().replaceAll(",","");
        let input2 = $("#input2").val().replaceAll(",","");
        let numberResult;
        let operation = Number($("#select").val());

        switch (operation) {
            case 1: numberResult = Number(input1) + Number(input2); break;

            case 2: numberResult = input1 - input2; break;

            case 3: numberResult = input1 * input2; break;

            case 4: numberResult = input1 / notZero(input2); break;
        }
        document.getElementById("result").innerText = formatNumber(numberResult.toString());

        console.log(formatNumber(Number(156465.15)))
    })
});
