/*DONE*/function firstLetterUpperCase(str){
   return str.toLowerCase().split(' ').map(function(word) {
      return word[0].toUpperCase() + word.substr(1);
   }).join(' ');
}  //this function is for word formattting

$(document).ready(function (){
   $("#add-button").click(function ()  // This function is for addFunction
   {
      $(".info-span").css("color", "#009879").text("Fill out all the inputs to add a new raw");
      var name = $("#name-full").val();
      var age = $("#age").val();
      var priceTotal = $("#price-total").val();
      var pricePaid = $("#price-paid").val();
      var doneYesNo = $("#select-done").val();

      if (name !== "" && age !== "" && priceTotal !== "" && pricePaid !== "" && doneYesNo !== "0")
      {
         $("tbody").append("<tr>\n" +
             "                <td><p>" + name + "</p></td>\n" +
             "                <td><p>" + age + "</p></td>\n" +
             "                <td><p>" + ("$ " + priceTotal) + "</p></td>\n" +
             "                <td><p>" + ("$ " + pricePaid) + "</p></td>\n" +
             "                <td><p>" + doneYesNo + "</p></td>\n" +
             "            </tr>");

         $("#name-full").val("");
         $("#age").val("");
         $("#price-paid").val("");
         $("#price-total").val("");
      }else if(name === "" || age === "" || priceTotal === "" || pricePaid === "" || doneYesNo === "0") {
         if ($("#remove-icon").css("display") !=="none") {
            $(".info-span").css("color", "red").text("Please fill out the all missing inputs!!!");
            if (name === "")
               $("#name-full").css("border", "1px solid red");
            if (age === "")
               $("#age").css("border", "1px solid red");
            if (priceTotal === "")
               $("#price-total").css("border", "1px solid red");
            if (pricePaid === "")
               $("#price-paid").css("border", "1px solid red");
            if (doneYesNo === "0")
               $("#select-done").css("border", "1px solid red");
         }
      }

      if ($("#remove-icon").css("display") === "none") {
         $(".input-hide-show, #edit-button, #delete-button, #remove-icon").toggle();
         $("table tbody tr").toggleClass("tr-hover-effect");
         $(".input-hide-show").addClass("background-color-adding");
         $("#remove-icon").addClass("add-icon-style");
      }

      if ($(".span-for-text-delete").html() === "")
         $(".span-for-text-delete").html("Add row").css("color", "#049f31");

   });

   $("#edit-button").click(function () // THis function is for editFunction
   {
      $(".info-span").css("color", "#ce7843").text("Double click the row you want to edit ");
      if ($("#remove-icon").css("display") === "none")
      {
         $("tbody tr").toggleClass("edit-animation");
         $("#add-button, #delete-button, #edit-icon-after, #remove-icon, #edit-button").toggle();
         $("#edit-icon-after, #remove-icon").addClass("edit-icon-style");

         if ($(".span-for-text-delete").html() !== "Edit row")
            $(".span-for-text-delete").html("Edit row").css("color", "#ea9538");

      }
   });

   $("#delete-button").click(function () // This function is for deleteFunction
   {
      $(".info-span").text("Double click the row you want to delete ").css("color", "#d22121");
      if ($("#remove-icon").css("display") === "none") {
         $("tbody tr").addClass("delete-animation");
         $("#add-button, #edit-button, #remove-icon, #delete-button").toggle();
         $("#delete-button").addClass("delete-icon-style");
      }

      if ($(".span-for-text-delete").html() !== "Delete")
         $(".span-for-text-delete").html("Delete").css("color", "#e54f23");

   });

   $("#remove-icon").click(function () // This function is for remove-icon to function differently (inside this function there are also comments for each funciton)
   {
      $(".info-span").text("");

      /* Remove-icon for addFunction */
      if ($("#edit-button").css("display") === "none" && $("#delete-button").css("display") === "none" && $("#edit-icon-after").css("display") === "none" && $(".span-for-text-delete").text() !== "Delete")
      {
         $(".input-hide-show, #edit-button, #delete-button, #remove-icon").toggle();
         $("table tbody tr").toggleClass("tr-hover-effect");
         $(".input-hide-show").toggleClass("background-color-adding");
         $("#remove-icon").toggleClass("add-icon-style");
         $("input, select").css("border", "none");
         $("#name-full, #age, #price-total, #price-paid").val("");

         if ($(".span-for-text-delete").html() !== "Add row")
            $(".span-for-text-delete").html("Add row").css("color", "#049f31");
         else
            $(".span-for-text-delete").html("");
      }

      /* Remove-icon for editFunction */
      else if ($("#add-button").css("display") === "none" && $("#delete-button").css("display") === "none" && $("#edit-icon-after").css("display") !== "none"){
         $("#add-button, #edit-button, #delete-button, #remove-icon, #edit-icon-after").toggle();
         $("tbody tr").removeClass("edit-animation");
         $("table tbody tr").addClass("tr-hover-effect");
         $("#remove-icon, #edit-cancel-icon").toggleClass("edit-icon-style");
         $("#edit-cancel-icon").toggleClass("edit-cancel-icon-animation");

         if ($(".span-for-text-delete").html() !== "Edit row")
            $(".span-for-text-delete").html("Edit row").css("color", "#ea9538");
         else
            $(".span-for-text-delete").html("");
      }

      /* Remove-icon for deleteFunction */
      else{
         $("tbody tr").toggleClass("delete-animation");
         $("#add-button, #edit-button, #remove-icon, #delete-button").toggle();
         $("#delete-button").toggleClass("delete-icon-style");
         $("#delete-button").css("color", "#009879!important");

         if ($(".span-for-text-delete").html() !== "Delete")
            $(".span-for-text-delete").html("Delete").css("color", "#e54f23");
         else
            $(".span-for-text-delete").html("");
      }
   });

   $("#name-full").on( // This function is for input field of name-full input that filter and style the name field
   {
      keypress: function (event){
         String.fromCharCode(event.keyCode);
         return ((event.charCode === 32) || (event.charCode > 64 &&
             event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)) && $(this).val().length <=50 ;
      },
      keyup: function (){
         var valueInput = $(this).val().replaceAll("  ", "");
         $(this).val(firstLetterUpperCase(valueInput));
      },
      change: function (){
         var valueInput = $(this).val().replaceAll("  ", "");
         $(this).val(firstLetterUpperCase(valueInput));
      }
   });

   $(document).on("keypress", "#name-full-1", function (event){
      String.fromCharCode(event.keyCode);
      return ((event.charCode === 32) || (event.charCode > 64 &&
          event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)) && $(this).val().length <=50 ;
   })

   $(document).on("keyup", "#name-full-1", function (){
      var valueInput = $(this).val().replaceAll("  ", "");
      $(this).val(firstLetterUpperCase(valueInput));
   })


   $("#age").keypress(function (e) //This function is for validation of age input field
   {

      if ( !isNaN( String.fromCharCode(e.keyCode) )){
         return true;
      }else
         return false;
   });

   $(document).on("keypress", "#age-1", function (event){
      let x = $("#age-1").val().replaceAll(" ", "");
      $("#age-1").val(x);
      if ( !isNaN( String.fromCharCode(event.keyCode) )){
         return true;
      }else
         return false;
   })

   $(document).on("click", "#edit-icon-after",  function ()
   {
      if ($("#remove-icon").css("display") === "none"){
         $("tbody tr").addClass("edit-animation");
         $("#edit-cancel-icon").toggleClass("edit-cancel-icon-animation");
         $("#remove-icon, #edit-button #edit-icon-after, #edit-cancel-icon").toggle();
         $(".info-span").css("color", "#ce7843").text("Double click the row you want to edit ");
         $(".span-for-text-delete").text("Edit row");

         var nameAfter = $("#name-full-1").val();
         var ageAfter = $("#age-1").val();
         var priceTotalAfter = $("#price-total-1").val().replaceAll("$","").replaceAll(",","").replaceAll(" ", "");
         var pricePaidAfter = $("#price-paid-1").val().replaceAll("$","").replaceAll(",","").replaceAll(" ", "");
         var yesNoAfter = $("#select-done-1").val();

         $("#edit-input-row").replaceWith(function () {
            return $("<tr class='tr-hover-effect edit-animation' />").append(
                "                <td><p>" + nameAfter + "</p></td>\n" +
                "                <td><p>" + ageAfter + "</p></td>\n" +
                "                <td><p>" + ("$ " + formatNumber(priceTotalAfter)) + "</p></td>\n" +
                "                <td><p>" + ("$ " + formatNumber(pricePaidAfter)) + "</p></td>\n" +
                "                <td><p>" + yesNoAfter + "</p></td>\n");
         });
      }
   });

   $(document).on("dblclick", "tr.edit-animation", function ()

   {
      $("#edit-cancel-icon").toggle().addClass("edit-icon-style, edit-cancel-icon-animation");
      $("#remove-icon").hide();
      $("tbody tr").toggleClass("edit-animation");
      var name = $(this).find("td:nth-child(1) p").text();
      var age = $(this).find("td:nth-child(2) p").text();
      var priceTotal = $(this).find("td:nth-child(3) p").text().replaceAll("$", "").replaceAll(",", "").replaceAll(" ", "");
      var pricePaid = $(this).find("td:nth-child(4) p").text().replaceAll("$", "").replaceAll(",", "").replaceAll(" ", "");
      var yesNo = $(this).find("td:nth-child(5) p").text();
      window.nameG = name;
      window.ageG =age;
      window.priceTotalG = priceTotal;
      window.pricePaidG = pricePaid;
      window.yesNoG = yesNo;

      $(this).replaceWith(function(){
         return $("<tr class='background-color-editing' id='edit-input-row' />").append("<td><input id=\"name-full-1\" class=\"input-style\" type=\"text\" value=\"" + `${name}` +  "\"" + " ></td>\n" +
             "                <td><input id=\"age-1\" class=\"input-style\" type=\"text\" value=\"" + `${age}` +  "\"" + "></td>\n" +
             "                <td><input id=\"price-total-1\" class=\"input-style\" type=\"text\" value=\"" + `${formatNumber(priceTotal.replaceAll("$", "").replaceAll(",", ""))}` +  "\"" + "></td>\n" +
             "                <td><input id=\"price-paid-1\" class=\"input-style\" type=\"text\" value=\"" + `${formatNumber(pricePaid.replaceAll("$", "").replaceAll(",", ""))}` +  "\"" + "></td>\n" +
             "                <td><select id=\"select-done-1\" style=\"height: 26px; color: #1c7430\">\n" +
             "                        <option value=\"0\" style=\"font-weight: bold; color: #1c7430\"></option>\n" +
             "                        <option value=\"Yes\" style=\"font-weight: bold; color: #1c7430\">Yes</option>\n" +
             "                        <option value=\"No\" style=\"font-weight: bold; color: #1c7430\">No</option>\n" +
             "                    </select>\n" +
             "                </td>\n");
      });
      $("select option").filter(function() {
         return $(this).text() == yesNo;
      }).prop('selected', true);
   });

   $(document).on("click", "#edit-cancel-icon", function ()
   {
               $("#edit-cancel-icon, #remove-icon").toggle();
               $(".background-color-editing").replaceWith(function(){
                  return $("<tr class='tr-hover-effect' />").append(
                      "                <td><p>" + nameG + "</p></td>\n" +
                      "                <td><p>" + ageG + "</p></td>\n" +
                      "                <td><p>" + ("$ " + formatNumber(priceTotalG)) + "</p></td>\n" +
                      "                <td><p>" + ("$ " + formatNumber(pricePaidG)) + "</p></td>\n" +
                      "                <td><p>" + yesNoG + "</p></td>\n");
               });
               $("tbody tr").toggleClass("edit-animation");
            });

   $(document).on("dblclick", "tr.delete-animation", function () //This is delete function after clicking double
   {
      $(this).remove();
   });
});